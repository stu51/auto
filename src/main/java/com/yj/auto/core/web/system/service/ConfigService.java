package com.yj.auto.core.web.system.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.SqlPara;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Service;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.jfinal.base.BaseCache;
import com.yj.auto.core.jfinal.base.BaseService;
import com.yj.auto.core.web.system.model.Config;

/**
 * Config 管理 描述：
 */
@Service(name = "configSrv")
public class ConfigService extends BaseService<Config> implements BaseCache {

	private static final Log logger = Log.getLog(ConfigService.class);

	public static final String SQL_LIST = "system.config.list";

	public static final Config dao = new Config().dao();

	@Override
	public Config getDao() {
		return dao;
	}

	@Override
	public SqlPara getListSqlPara(QueryModel query) {
		if (StrKit.isBlank(query.getOrderby())) {
			query.setOrderby("sort");
		}
		return getSqlPara(SQL_LIST, query);
	}

	// 根据配置代码查询
	public Config getByCode(String code) {
		String sql = "select * from t_sys_config where code=?";
		return dao.findFirst(sql, code);
	}

	@Override
	public void loadCache() {
		QueryModel query = new QueryModel();
		query.setOrderby("sort");
		query.setState(Constants.DATA_STATE_VALID);
		List<Config> list = find(SQL_LIST, query);
		for (Config r : list) {
			addCache(r.getCode(), r);
		}
	}

	@Override
	public String getCacheName() {
		return Constants.CACHE_NAME_CONFIG;
	}
}