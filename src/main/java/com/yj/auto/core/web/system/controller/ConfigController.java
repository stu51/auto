package com.yj.auto.core.web.system.controller;

import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Controller;
import com.yj.auto.core.base.annotation.Valid;
import com.yj.auto.core.base.model.QueryModel;
import com.yj.auto.core.base.model.ResponseModel;
import com.yj.auto.core.jfinal.base.BaseController;
import com.yj.auto.core.web.system.model.Config;
import com.yj.auto.core.web.system.service.ConfigService;
import com.yj.auto.plugin.table.model.DataTables;

/**
 * 系统配置 管理
 * 
 * 描述：
 * 
 */
@Controller(viewPath = "system")
public class ConfigController extends BaseController {
	private static final Log logger = Log.getLog(ConfigController.class);

	private static final String RESOURCE_URI = "config/index";
	private static final String INDEX_PAGE = "config_index.html";
	private static final String FORM_PAGE = "config_form.html";
	private static final String SHOW_PAGE = "config_show.html";

	ConfigService configSrv = null;

	public void index() {
		render(INDEX_PAGE);
	}

	public void list(QueryModel query) {
		DataTables<Config> dt = getDataTable(query, configSrv);
		renderJson(dt);
	}

	public void get() {
		Integer id = getParaToInt(0);
		Config model = configSrv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}

	public void form() {
		Integer id = getParaToInt(0);
		Config model = null;
		if (null != id && 0 != id) {
			model = configSrv.get(id);
		} else {
			model = new Config();
			model.setType("01");
			model.setSort(10);
			model.setState(Constants.DATA_STATE_VALID);
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = Config.class)
	public boolean saveOrUpdate(Config model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			if (null != configSrv.getByCode(model.getCode())) {
				ResponseModel<String> res = new ResponseModel<String>(false);
				res.setMsg("配置代码[" + model.getCode() + "]已存在!");
				renderJson(res);
				return false;
			}
			success = configSrv.save(model);
		} else {
			success = configSrv.update(model);
		}
		ResponseModel<String> res = renderSaveOrUpdate(success, configSrv);
		return res.isSuccess();
	}

	public boolean delete() {
		ResponseModel<String> res = _delete(configSrv);
		return res.isSuccess();
	}
}