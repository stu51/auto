package com.yj.auto.core.web.system.model;

import com.yj.auto.Constants;
import com.yj.auto.core.base.annotation.Table;
import com.yj.auto.core.web.system.model.bean.AttaEntity;

/**
 * 系统附件表
 */
@SuppressWarnings("serial")
@Table(name = Atta.TABLE_NAME, key = Atta.TABLE_PK, remark = Atta.TABLE_REMARK)
public class Atta extends AttaEntity<Atta> {
	// 转json的时候，必须先调用一下此方法
	public String getUri() {
		String uri = null;
		if (null != getId()) {
			uri = com.yj.auto.Constants.ATTA_DOWNLOAD_ACTION + getId();
		}
		put("uri", uri);
		return uri;
	}

	public String getAbsolutePath() {
		String path = Constants.SYS_UPLOAD_ROOT + getPath();
		return path;
	}
}