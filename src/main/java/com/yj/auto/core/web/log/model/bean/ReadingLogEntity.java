package com.yj.auto.core.web.log.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.*;

/**
 *  阅读记录
 */
@SuppressWarnings("serial")
public abstract class ReadingLogEntity<M extends ReadingLogEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_log_reading"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "阅读记录"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：user_id
	 * @return 用户主键
	 */
   	@NotNull 	
	public Integer getUserId(){
   		return get("user_id");
   	}
	
	public void setUserId(Integer userId){
   		set("user_id" , userId);
   	}	
   		
	/**
	 * Column ：type
	 * @return 业务类型
	 */
   	@NotBlank
	@Length(max = 64)	
	public String getType(){
   		return get("type");
   	}
	
	public void setType(String type){
   		set("type" , type);
   	}	
   		
	/**
	 * Column ：data_id
	 * @return 业务主键
	 */
   	@NotNull 	
	public Integer getDataId(){
   		return get("data_id");
   	}
	
	public void setDataId(Integer dataId){
   		set("data_id" , dataId);
   	}	
   		
	/**
	 * Column ：read_time
	 * @return 阅读时间
	 */
   	@NotNull 	
	public Date getReadTime(){
   		return get("read_time");
   	}
	
	public void setReadTime(Date readTime){
   		set("read_time" , readTime);
   	}	
}