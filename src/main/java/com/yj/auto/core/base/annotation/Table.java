package com.yj.auto.core.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.yj.auto.Constants;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Table {

	/**
	 * 表名
	 * 
	 * @return
	 */
	String name();

	/**
	 * 主键
	 * 
	 * @return
	 */
	String key() default "ID";

	/**
	 * 注释
	 * 
	 * @return
	 */
	String remark();

	/**
	 * 数据库名称
	 * 
	 * @return
	 */
	String dbname() default Constants.DATABASE_NAME;
}
