package com.yj.auto.core.base.model;

import com.yj.auto.Constants;

public class ResponseModel<T> {
	private boolean success = false;
	private String code;
	private String msg = Constants.OP_MSG_FAIL;

	private T data;

	public ResponseModel() {

	}

	public ResponseModel(boolean success) {
		super();
		this.success = success;
		if (success) {
			msg = Constants.OP_MSG_SUCCESS;
		}
	}

	public ResponseModel(boolean success, String msg) {
		super();
		this.success = success;
		this.msg = msg;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
