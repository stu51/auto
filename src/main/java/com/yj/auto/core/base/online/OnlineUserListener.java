package com.yj.auto.core.base.online;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import com.yj.auto.helper.LogHelper;

public class OnlineUserListener implements HttpSessionBindingListener {

	private SessionUser su = null;

	public OnlineUserListener(SessionUser su) {
		this.su = su;
	}

	public void valueBound(HttpSessionBindingEvent e) {
		OnlineContext.add(e.getSession());
		// System.err.println("sessionId = " + e.getSession().getId());
	}

	public void valueUnbound(HttpSessionBindingEvent e) {
		String sessionId = e.getSession().getId();
		OnlineContext.remove(sessionId);
		LogHelper.addLogoutLog(sessionId);
	}

}
