package com.yj.auto.plugin.ztree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ZTree节点数据封装
 */
public class ZTreeNode {

	/**
	 * 节点id
	 */
	private String id;

	/**
	 * 节点名称
	 */
	private String name;

	/**
	 * 是否上级节点
	 */
	private boolean isParent;

	/**
	 * 是否选中
	 */
	private boolean checked;

	/**
	 * 是否选中
	 */
	private boolean nocheck;

	/**
	 * 是否展开
	 */
	private boolean open;
	/**
	 * 节点图标
	 */
	private String icon;

	/**
	 * 子节点数据
	 */
	private List<ZTreeNode> children;

	/**
	 * 扩展属性
	 */
	private Map<String, Object> attributes = new HashMap<String, Object>();

	public ZTreeNode(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getIsParent() {
		return isParent;
	}

	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isNocheck() {
		return nocheck;
	}

	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public List<ZTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<ZTreeNode> children) {
		this.children = children;
	}

	public void addChildren(ZTreeNode node) {
		if (null == this.children) {
			this.children = new ArrayList<ZTreeNode>();
		}
		children.add(node);
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void addAttribute(String key, Object value) {
		this.attributes.put(key, value);
	}

	public void addAttributes(Map<String, Object> attr) {
		this.attributes.putAll(attr);
	}
}
